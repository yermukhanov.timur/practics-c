
#include <iostream>
#include <Math.h>

class Animal {
	public:
	Animal() {}
	virtual void Voice () {
		std::cout << "Text";
	}
};

class Cat: public Animal {
	public:
		Cat() {}
		void Voice() override {
			std::cout << "Meow \n";
		}
};

class Dog : public Animal {
public:
	Dog() {}
	void Voice() override {
		std::cout << "Wow \n";
	}
};

class Cow : public Animal {
public:
	Cow() {}
	void Voice() override {
		std::cout << "MUU \n";
	}
};


int main()
{
	Animal * AllAnimals[3];
	AllAnimals[0] = new Cat;
	AllAnimals[1] = new Dog;
	AllAnimals[2] = new Cow;

	for (int i = 0; i < 3; i++) {
		AllAnimals[i]->Voice();
	}
	
}

